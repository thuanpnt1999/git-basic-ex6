B1:git log (xem vị trí các commit)
B2: git rebase -i HEAD~2 (hiện ra giao diện từ commit hiện tại đến trước đó 2 commit bao gồm accident commit)
B3: sửa từ pick tại accident commit thành edit rồi thoát ra khỏi giao diện, bây giờ HEAD đã trỏ tới accident commit.
B4: undo lại những thứ đã làm tại accident commit
B5: git add .
B6: git commit --amend (sửa commit nhưng giữ nguyên lịch sử commit)
B7: git rebase --continue (HEAD quay về vị trí ban đầu)
(nếu xảy ra conflict thì vào file thực hiện thay đổi sửa lại rồi gõ lệnh 
git add .
git rebase --continue
